/* eslint-disable no-unused-vars */
const { v4: uuidv4 } = require('uuid');
const Service = require('./Service');
const toDoItems = [];

/**
* Creates a new to do item
*
* toDoItem ToDoItem To do item to create
* returns ToDoItem
* */
const createOne = ({ toDoItem }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse(
        {...toDoItems.push({ ...toDoItem, id: uuidv4(), status: 'pending' })},
      ));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);
/**
* Deletes a to do item by id
*
* id UUID ID of to do item to delete
* no response value expected for this operation
* */
const deleteOne = ({ id }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse(
        toDoItems.splice(toDoItems.findIndex(item => item.id === id), 1),
      ));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);
/**
* Returns all to do items. 
*
* limit Integer maximum number of results to return (optional)
* returns List
* */
const getMany = ({ limit }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse(
        toDoItems.slice(0, limit),
      ));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);
/**
* Returns a to do item by id
*
* id UUID ID of to do item to return
* returns ToDoItem
* */
const getOne = ({ id }) => new Promise(
  async (resolve, reject) => {
    try {
      resolve(Service.successResponse(
        toDoItems.find(item => item.id === id),
      ));
    } catch (e) {
      reject(Service.rejectResponse(
        e.message || 'Invalid input',
        e.status || 405,
      ));
    }
  },
);

module.exports = {
  createOne,
  deleteOne,
  getMany,
  getOne,
};