import ReactDOM from 'react-dom';
import { useState } from 'react';
import { ToDoApi, ToDoItem }  from './src/index';

const App = () => {
  const apiInstance = new ToDoApi();
  const [limit, setLimit] = useState(0);
  const [toDoIdToGet, setToDoIdToGet] = useState('');
  const [toDoIdToDelete, setToDoIdToDelete] = useState('');
  const [createOneResponse, setCreateOneResponse] = useState('');
  const [getManyResponse, setGetManyResponse] = useState('');
  const [getOneResponse, setGetOneResponse] = useState('');
  const [deleteOneResponse, setDeleteOneResponse] = useState('');

  const createOne = () => {
    const toDoItem = new ToDoItem();
    toDoItem.name = `item-${new Date().getTime()}`

    apiInstance.createOne(toDoItem, (error, data) => {
      setCreateOneResponse(error ? error.message : JSON.stringify(data));
      if (!error) setLimit((currentLimit) => currentLimit + 1);
    });
  }

  const getMany = () => {
    apiInstance.getMany({ limit }, (error, data) => {
      setGetManyResponse(error ? error.message : JSON.stringify(data));
    });
  }

  const getOne = () => {
    apiInstance.getOne(toDoIdToGet, (error, data) => {
      setGetOneResponse(error ? error.message : JSON.stringify(data));
      setToDoIdToGet('');
    });
  }

  const deleteOne = () => {
    apiInstance.deleteOne(toDoIdToDelete, (error, data) => {
      setDeleteOneResponse(error ? error.message : JSON.stringify(data));
      setToDoIdToDelete('');
      if (!error) setLimit((currentLimit) => currentLimit - 1);
    });
  }

  const onChangeToDoIdToGet = ({ target: { value } }) => {
    setToDoIdToGet(() => value);
  };

  const onChangeToDoIdToDelete = ({ target: { value } }) => {
    setToDoIdToDelete(() => value);
  };

  return (
    <div
      style={{
        display: 'grid',
        gridRow: 2,
        gridColumn: 6,
        gridTemplateColumns: '1fr 1fr 1fr 1fr 1fr 1fr',
        gridTemplateRows: 'auto 50vh',
        gridColumnGap: 10,
        gridRowGap: 10
      }}
    >
      <button
        style={{
          gridColumnStart: 1
        }}
        onClick={createOne}
      >
        Create to do
      </button>
      <textarea
        style={{
          gridColumnStart: 1,
          gridRowStart: 2
        }}
        readOnly
        value={createOneResponse}
      />
      <button
        style={{
          gridColumnStart: 2
        }}
        onClick={getMany}
      >
        Get Many
      </button>
      <textarea
        style={{
          gridColumnStart: 2,
          gridRowStart: 2
        }}
        readOnly
        value={getManyResponse}
      />
      <button
        style={{
          gridColumnStart: 3
        }}
        onClick={getOne}
      >
        Get One
      </button>
      <input
        style={{
          gridColumnStart: 4
        }}
        onChange={onChangeToDoIdToGet}
        type='text'
        placeholder='To do id'
        value={toDoIdToGet}
      />
      <textarea
        style={{
          gridColumnStart: 3,
          gridColumnEnd: 5,
          gridRowStart: 2
        }}
        readOnly
        value={getOneResponse}
      />
      <button
        style={{
          gridColumnStart: 5
        }}
        onClick={deleteOne}
      >
        Delete One
      </button>
      <input
        style={{
          gridColumnStart: 6
        }}
        onChange={onChangeToDoIdToDelete}
        type='text'
        placeholder='To do id'
        value={toDoIdToDelete}
      />
      <textarea
        style={{
          gridColumnStart: 5,
          gridColumnEnd: 7,
          gridRowStart: 2
        }}
        readOnly
        value={deleteOneResponse}
      />
    </div>
  );
}

const app = document.getElementById('app');
ReactDOM.render(<App />, app);